/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Routes } from '@angular/router';

import { NgxLoginComponent } from './login/login.component';
import { NgxRegisterComponent } from './register/register.component';

export const authRoutes: Routes = [
  {
    path: 'login',
    component: NgxLoginComponent,
  },
  {
    path: 'register',
    component: NgxRegisterComponent,
  },
];
