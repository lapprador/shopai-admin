/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { NbAuthSocialLink, NbAuthService, NB_AUTH_OPTIONS, NbAuthResult, NbAuthJWTToken, NbTokenService } from '@nebular/auth';
import { getDeepFromObject } from '@nebular/auth/helpers';

import { ApiService } from '../../../@core/data/api.service';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NgxLoginComponent {

  redirectDelay: number = 0;
  showMessages: any = {};
  strategy: string = '';

  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  submitted: boolean = false;
  socialLinks: NbAuthSocialLink[] = [];
  rememberMe = false;

  hostRegex = /([a-z0-9|-]+\.)*[a-z0-9|-]+\.[a-z]+/gm;
  shopifyHost: string = '';

  loginIsLoading = false;

  constructor(protected service: NbAuthService,
              @Inject(NB_AUTH_OPTIONS) protected options = {},
              protected cd: ChangeDetectorRef,
              protected router: Router,
              private api: ApiService,
              private activatedRoute: ActivatedRoute,
              private tokenService: NbTokenService
              ) {

    this.redirectDelay = this.getConfigValue('forms.login.redirectDelay');
    this.showMessages = this.getConfigValue('forms.login.showMessages');
    this.strategy = this.getConfigValue('forms.login.strategy');
    this.socialLinks = this.getConfigValue('forms.login.socialLinks');
    this.rememberMe = this.getConfigValue('forms.login.rememberMe');

    this.activatedRoute.queryParams.subscribe(params => {

      // If redirected from auth flow
      if("auth" in params) {
        let t = new NbAuthJWTToken(params["auth"], "custom");
        this.tokenService.set(t);
        this.router.navigate(['/'])
      }

      else if("shopify-host" in params) {
        this.shopifyHost = params["shopify-host"]
        this.installButtonClicked()
      }

    });

  }

  login(): void {
    this.errors = [];
    this.messages = [];
    this.submitted = true;

    this.service.authenticate(this.strategy, this.user).subscribe((result: NbAuthResult) => {
      this.submitted = false;

      if (result.isSuccess()) {
        this.messages = result.getMessages();
      } else {
        this.errors = result.getErrors();
      }

      const redirect = result.getRedirect();
      if (redirect) {
        setTimeout(() => {
          return this.router.navigateByUrl(redirect);
        }, this.redirectDelay);
      }
      this.cd.detectChanges();
    });
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }

  // Shopify login logic
  shopURLValid() {
      return this.shopifyHost.match(this.hostRegex) != null
  }

  installButtonClicked() {
    if(!this.shopURLValid() || this.loginIsLoading) { return }
    this.loginIsLoading = true;
    this.api.genShopifyAuthURL(this.shopifyHost.match(this.hostRegex)[0]).subscribe(res => {
      // Redirect to url
      window.location.href = res.url;
    });
  }


}
