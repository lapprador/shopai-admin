import { NgxStepperComponent } from './stepper.component';
import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: 'button[ngxStepperNext]',
})
export class NgxStepperNextDirective {

  @Input() @HostBinding('attr.type') type: string = 'submit';

  constructor(private stepper: NgxStepperComponent) {
  }

  @HostListener('click')
  onClick() {
    this.stepper.next();
  }
}

@Directive({
  selector: 'button[ngxStepperPrevious]',
})
export class NgxStepperPreviousDirective {

  @Input() @HostBinding('attr.type') type: string = 'button';

  constructor(private stepper: NgxStepperComponent) {
  }

  @HostListener('click')
  onClick() {
    this.stepper.previous();
  }
}
