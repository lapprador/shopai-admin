import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';

import { RouterModule }   from '@angular/router';

import { NgxLoginComponent } from './auth/login/login.component';
import { NgxRegisterComponent } from './auth/register/register.component';


import { NbSharedModule } from '@nebular/theme/components/shared/shared.module';
import { NgxStepperComponent } from './theme/stepper/stepper.component';
import { NgxStepComponent } from './theme/stepper/step.component';
import { NgxStepperNextDirective, NgxStepperPreviousDirective } from './theme/stepper/stepper-button.directive';

import {
    NbAlertModule,
    NbCheckboxModule,
    NbButtonModule,
    NbCardModule,
    NbInputModule,
    NbLayoutModule,
    NbThemeModule,
    NbSpinnerModule,
} from '@nebular/theme';

import { authRoutes } from './auth/auth.routes'

import { HttpClientModule, HttpRequest } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(authRoutes),
    NbAlertModule,
    NbCheckboxModule,
    NbButtonModule,
    NbCardModule,
    NbInputModule,
    NbLayoutModule,
    NbSpinnerModule,
  ],
  declarations: [
      NgxLoginComponent,
      NgxRegisterComponent,
      NgxStepperComponent,
      NgxStepComponent,
      NgxStepperNextDirective,
      NgxStepperPreviousDirective,

  ],
  exports: [
      NgxLoginComponent,
      NgxRegisterComponent,
      NgxStepperComponent,
      NgxStepComponent,
      NgxStepperNextDirective,
      NgxStepperPreviousDirective,
  ]
})
export class CustomNebularModule { }
