import { CustomNebularModule } from './custom-nebular.module';

describe('CustomNebularModule', () => {
  let customNebularModule: CustomNebularModule;

  beforeEach(() => {
    customNebularModule = new CustomNebularModule();
  });

  it('should create an instance', () => {
    expect(customNebularModule).toBeTruthy();
  });
});
