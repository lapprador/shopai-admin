import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { NbAuthService } from '@nebular/auth';

import { map } from 'rxjs/operators/map';
import { tap } from 'rxjs/operators/tap';

import { Observable } from 'rxjs';

import { ApiService } from 'app/@core/data/api.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: NbAuthService, private router: Router, private api: ApiService) {
  }

  canActivate() {

    let whatToDo = new Observable(observer => {
      this.authService.isAuthenticated().subscribe(authenticated => {

        if(!authenticated) {
          observer.next({authenticated: authenticated, userState: null})
        } else {

          this.api.getUser(true).subscribe(userInfo => {
            observer.next({authenticated: authenticated, userState: userInfo["state"]});
          })

        }

      });
    })

    return whatToDo
      .pipe(
        tap(facts => {

          let authenticated = facts["authenticated"];
          let userState = facts["userState"];

          if(!authenticated) {
            this.router.navigate(['auth/login']);
            return;
          }

          if(userState["shop_connected"] !== true) {
            this.router.navigate(['intro']);
            return;
          }

          if(userState["has_sliders"] !== true) {
            this.router.navigate(['intro']);
            return;
          }

        }),
        map(x => {
          return (
            x["authenticated"] &&
            x["userState"]["shop_connected"] === true &&
            x["userState"]["has_sliders"] === true
          )
        })
      );

    // this.authService.isAuthenticated()
    // return this.authService.isAuthenticated()
    //   .pipe(
    //     tap(authenticated => {
    //       if (!authenticated) {
    //         this.router.navigate(['auth/login']);
    //       }
    //       else if(this.api.isUserOnboarded()) {
    //         this.router.navigate(['auth/register']);
    //       }
    //     }),
    //   );
  }
}
