import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {
  NbAuthComponent,
  NbLoginComponent,
  NbLogoutComponent,
  NbRegisterComponent,
  NbRequestPasswordComponent,
  NbResetPasswordComponent,
} from '@nebular/auth';

import { NgxLoginComponent } from './@nebular/auth/login/login.component';
import { NgxRegisterComponent } from './@nebular/auth/register/register.component';

import { IntroComponent } from './intro/intro.component'

import { AuthGuard } from './auth-guard.service';

import { CustomNebularModule } from './@nebular/custom-nebular.module'

const routes: Routes = [
  { path: 'p', loadChildren: 'app/pages/pages.module#PagesModule', canActivate: [AuthGuard], },
  {
    path: 'auth',
    component: NbAuthComponent,
    children: [
      {
        path: '',
        component: NgxLoginComponent,
      },
      {
        path: 'login',
        component: NgxLoginComponent,
      },
      {
        path: 'register',
        component: NgxRegisterComponent,
      },
      {
        path: 'logout',
        component: NbLogoutComponent,
      },
      {
        path: 'request-password',
        component: NbRequestPasswordComponent,
      },
      {
        path: 'reset-password',
        component: NbResetPasswordComponent,
      },
    ],
  },
  {
    path: 'intro',
    component: IntroComponent
  },
  { path: '', redirectTo: 'p', pathMatch: 'full' },
  { path: '**', redirectTo: 'p' },
];

const config: ExtraOptions = {
  useHash: true,
};

@NgModule({
  imports: [CustomNebularModule, RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
