import { AfterViewInit, Component, OnDestroy, Input } from '@angular/core';
import { NbThemeService } from '@nebular/theme';

@Component({
  selector: 'ngx-echarts-bar',
  template: `
    <div echarts [options]="options" class="echart"></div>
  `,
})
export class EchartsBarComponent implements AfterViewInit, OnDestroy {
  options: any = {};
  themeSubscription: any;

  private _data = [];
  @Input('data')
  set data(val) {
    this._data = val;
    this.updateChart();
  }
  get data() {return this._data}

  constructor(private theme: NbThemeService) {
  }

  updateChart() {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const echarts: any = config.variables.echarts;

      // let series = this.y.map(arr => {
      //   return {
      //     name: 'Score',
      //     type: 'bar',
      //     barWidth: '60%',
      //     data: arr,
      //   }
      // });

      let labels = this.data.map(x=>x.x)

      let series = {
          name: 'Score',
          type: 'bar',
          barWidth: '60%',
          data: this.data.map(x=>x.y),
       }

      this.options = {
        backgroundColor: echarts.bg,
        color: [colors.primaryLight],
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'shadow',
          },
        },
        grid: {
          left: '3%',
          right: '4%',
          bottom: '3%',
          containLabel: true,
        },
        xAxis: [
          {
            type: 'category',
            data: labels,
            axisTick: {
              alignWithLabel: true,
            },
            axisLine: {
              lineStyle: {
                color: echarts.axisLineColor,
              },
            },
            axisLabel: {
              textStyle: {
                color: echarts.textColor,
              },
            },
          },
        ],
        yAxis: [
          {
            type: 'value',
            axisLine: {
              lineStyle: {
                color: echarts.axisLineColor,
              },
            },
            splitLine: {
              lineStyle: {
                color: echarts.splitLineColor,
              },
            },
            axisLabel: {
              textStyle: {
                color: echarts.textColor,
              },
            },
          },
        ],
        series: series,
      };
    });
  }

  ngAfterViewInit() {
    this.updateChart()
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
