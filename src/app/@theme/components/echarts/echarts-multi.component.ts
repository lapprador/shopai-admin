import { AfterViewInit, Component, Input } from '@angular/core';
import { NbThemeService } from '@nebular/theme';

import { EChartOption } from 'echarts';


declare var ecStat;

@Component({
  selector: 'ngx-echarts-multi',
  template: `
    <div echarts class="echart" [options]="options" (chartInit)="onChartInit($event)"></div>
  `,
})
export class EchartsMultiComponent implements AfterViewInit {

  chart = null

  echarts = {
    bg: '#ffffff',
    textColor: '#484848',
    axisLineColor: '#bbbbbb',
    splitLineColor: '#ebeef2',
    itemHoverShadowColor: 'rgba(0, 0, 0, 0.5)',
    tooltipBackgroundColor: '#6a7985',
    areaOpacity: '0.7',
  }

  options: EChartOption = {
      backgroundColor: this.echarts.bg,
      color: ["#2980b9", "#e67e22", "#e74c3c", "#1abc9c", "#f1c40f", "#27ae60", "#9b59b6", "#FDA7DF", "#cc8e35", "#aaa69d"],
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow',
        },
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true,
      },
      xAxis: [],
      yAxis: [],
      series: [],
  };

  @Input('chartType') chartType: string = 'bar';
  @Input('percentage') percentage: boolean = false;
  @Input('name') name: string = 'Value';
  @Input('color') color: string = '#8a7fff';

  private _data = null;
  @Input('data')
  set data(val) {
    this._data = val;
    this.updateForData();
  }
  get data() {return this._data}

  constructor() { }

  getOptions() {
    return this.options
  }

  newX(type, values=null) {
      var x =  {
        type: type,
        axisTick: {
          alignWithLabel: true,
        },
        axisLine: {
          lineStyle: {
            color: this.echarts.axisLineColor,
          },
        },
        axisLabel: {
          textStyle: {
            color: this.echarts.textColor,
          },
        },
    }

    if(values) {
      x["data"] = values;
    }
    return x;
  }

  newY(type, values=null, percentageFormat=false) {
    var y = {
      type: type,
      axisLine: {
        lineStyle: {
          color: this.echarts.axisLineColor,
        },
      },
      splitLine: {
        lineStyle: {
          color: this.echarts.splitLineColor,
        },
      },
      axisLabel: {
        textStyle: {
          color: this.echarts.textColor,
        },
      },
    }

    y.axisLabel["formatter"] = function(val) {
        if(percentageFormat) {
          return (val*100).toFixed(0) + '%';
        } else {
          return val.toFixed(0);
        }
    };


    if(values) {
      y["data"] = values;
    }
    return y;
  }

  newSeries(name, type, data, percentage=false, yIndex=0) {
    return {
        name: name,
        type: type,
        barWidth: '60%',
        lineStyle: {
          'width': 5
        },
        radius: '100%',
        data: data,
        yAxisIndex:yIndex,
        tooltip: {
           formatter: function(params) {
             var value = params.value;

             if(typeof(value) === "object") {
               value = value.length == 2 ? value[1] : value[0]
             }

             if(percentage === true) {
               value = (value * 100).toFixed(1) + '%'
             } else {
               value = value.toFixed(2);
             }
             return params.name + ": " + value;
           }
        },
     }
  }

  defaultAxis(xType, yType, xValues=null, yValues=null, percentageAxis=false) {
    this.reset();
    this.addXAxis(this.newX(xType, xValues));
    this.addYAxis(this.newY(yType, yValues, percentageAxis));
  }

  onChartInit(event) {
    this.chart = event
    this.refreshChart()
  }

  refreshChart() {
    if(this.chart != null) {
      this.chart.setOption(this.options)
    }
  }

  reset() {
    this.options.xAxis = [];
    this.options.yAxis = [];
    this.options.series = [];
  }

  addXAxis(x) {
    this.options.xAxis.push(x);

  }

  addYAxis(y) {
    this.options.yAxis.push(y);

  }

  addSeries(s, trend=null) {

    if(s.type === 'pie') {
      s.data = s.data.map(x => {
        return {name: x[0], value: x[1]}
      });
    }

    this.options.series.push(s);

    if(trend != null) {

      // Do the regression
      var reg = ecStat.regression(trend, s.data);

      // Sort from low to high
      reg.points.sort(function(a, b) {
          return a[0] - b[0];
      });

      var sTrend = this.newSeries("Trendline", 'line', [reg.points[0], reg.points[reg.points.length-1]]);
      sTrend["smooth"] = true;
      this.options.series.push(sTrend);
    }

    this.refreshChart();
  }

  updateForData() {
    if(this._data != null) {
      let x = this.data.map(x=>x.x)
      let y = this.data.map(x=>x.y)

      this.reset();
      this.addXAxis(this.newX('category', x));

      this.addYAxis(this.newY('value', null, this.percentage))

      this.addSeries(this.newSeries(this.name, this.chartType, y, this.percentage))
    }

  }

  ngAfterViewInit() { }

}
