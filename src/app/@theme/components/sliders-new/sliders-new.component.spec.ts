import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlidersNewComponent } from './sliders-new.component';

describe('SlidersNewComponent', () => {
  let component: SlidersNewComponent;
  let fixture: ComponentFixture<SlidersNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlidersNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlidersNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
