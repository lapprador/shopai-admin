import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { NgxStepperComponent } from '../../../@nebular/theme/stepper/stepper.component';

import { NbButtonComponent } from '@nebular/theme'

import { ApiService } from '../../../@core/data/api.service';

@Component({
  selector: 'ngx-sliders-new',
  templateUrl: './sliders-new.component.html',
  styleUrls: ['./sliders-new.component.scss']
})
export class SlidersNewComponent implements OnInit {

  @ViewChild("nameButton") nameNextButton: NbButtonComponent
  @ViewChild("stepper") stepper: NgxStepperComponent

  categories = []
  algorithms = {}

  filledForm = {
      category: null,
      algorithm: null,
      title: ""
  }

  createdSlider = null;

  constructor(public api: ApiService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    this.api.getAllAlgorithms().subscribe(res => {
      this.categories = res["categories"].reduce((rows, key, index) => (index % 2 == 0 ? rows.push([key]) : rows[rows.length-1].push(key)) && rows, []);
      this.algorithms = res["algorithms"];
    });

    this.activatedRoute.queryParams.subscribe(params => {
      console.log(params)
        if("token" in params && "xpath" in params) {
          this.stepper.selectedIndex = 5;
          console.log("HAD THEM")
          // Send result for slider
          this.api.setSliderLocation(params["token"], params["xpath"]).subscribe(res => {
            console.log(res)
          });
        }
    });

  }

  algorithmsForCategory(category) {
    if(!category) {
      return []
    }
    return category.algorithm_identifiers.map(x => this.algorithms[x]);
  }

  categoryClicked(category) {
    if(category.active === false) {
      return;
    }
    this.filledForm.category = category;
    this.stepper.next();
  }

  algorithmClicked(algorithm) {
    this.filledForm.algorithm = algorithm;
    this.stepper.next();
  }

  nameChanged(target, toValue) {
    this.filledForm.title = toValue;
  }

  isNameValid() {
    return this.filledForm.title.length >= 5;
  }

  saveLoading = false

  saveSlider() {

    if(this.saveLoading) {
      return
    }

    this.saveLoading = true;
    this.api.updateSlider(
        {
          "slider": {
            "title": this.filledForm.title,
            "algorithms_category": this.filledForm.category.id,
            "algorithm": this.filledForm.algorithm.identifier,
          }
        }
      ).subscribe(res => {
        this.saveLoading = false;
        this.createdSlider = res;
        this.stepper.next();
      })

  }

  choosePosition() {
    this.api.getSliderLocationURL(this.createdSlider["id"]).subscribe(res => {
      window.location.href = res.url;
    })
  }

  doneLoading = false;
  done() {
    this.doneLoading = true;
    this.router.navigate(['/p/sliders/overview'])
  }

}
