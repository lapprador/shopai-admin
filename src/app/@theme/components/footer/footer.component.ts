import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by" style="text-align: center; width: 100%;">If you have any feedback, even the smallest idea or problem, please write to our CEO at: <a href="mailto:Andreas@butlerhub.io"><b>Andreas@butlerhub.io</b></a> 💡🙌</span>

  `,
})
export class FooterComponent {
}
    // <div class="socials">
    //   <a href="#" target="_blank" class="ion ion-social-github"></a>
    //   <a href="#" target="_blank" class="ion ion-social-facebook"></a>
    //   <a href="#" target="_blank" class="ion ion-social-twitter"></a>
    //   <a href="#" target="_blank" class="ion ion-social-linkedin"></a>
    // </div>