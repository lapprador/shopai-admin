import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { Router } from '@angular/router';

import { SlidersNewComponent } from '../@theme/components'

import { ApiService } from 'app/@core/data/api.service';

declare var window;

@Component({
  selector: 'ngx-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {

  @ViewChild('urlInput') urlInput: ElementRef

  constructor(private api: ApiService, private router: Router) { }

  regex = /([a-z0-9|-]+\.)*[a-z0-9|-]+\.[a-z]+/gm;

  domain = "";

  isLoading = false;

  introState = null;

  ngOnInit() {

    this.api.getUser(true).subscribe(userInfo => {
      console.log(userInfo)

      let s = userInfo["state"]

      if(s["shop_connected"] === false) {
        this.introState = "shop";
      }
      else if(s["has_sliders"] === false) {
        this.introState = "slider";
      } else {
        // Redirect
        this.router.navigate(['/'])
      }

    });

  }

  installButtonClicked() {
    if(!this.shopURLValid()) { return }

    this.isLoading = true;

    this.api.genShopifyAuthURL(this.domain.match(this.regex)[0]).subscribe(res => {
      // Redirect to url
      console.log(res.url);
      window.location.href = res.url;
    });

    console.log("Click")
  }

  URLChanged(target, toValue) {
    this.domain = toValue;
  }

  shopURLValid() {
      return this.domain.match(this.regex) != null
  }

}
