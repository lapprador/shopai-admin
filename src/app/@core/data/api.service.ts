import { Injectable } from '@angular/core';

import { HttpClient, HttpParams } from '@angular/common/http';

import { NbAuthService, NbAuthJWTToken, NbAuthJWTInterceptor } from '@nebular/auth'

import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

import {tap} from 'rxjs/operators/tap';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

    baseURL = "https://api.butlerhub.io";
    // baseURL = "http://127.0.0.1:4567";

    jwt = null;

  constructor(private http: HttpClient, private authService: NbAuthService) {
      this.authService.onTokenChange()
      .subscribe((token: NbAuthJWTToken) => {
          this.jwt = token;
      });
  }

  params(toSet) {
      var res = new HttpParams()

      Object.keys(toSet).forEach(key => {
          res = res.set(key, toSet[key]);
      });

      return {"params": res};
  }

  getUser(update=false) {
    let user = localStorage.getItem('user');

    if(user == null || update) {
      return this.http.get(this.baseURL + '/admin/me').pipe(
         tap( x => this.setUser(x))
       );
    }

    return new Observable(observer => {
      observer.next(JSON.parse(user))
    })
  }

  setUser(user) {
    localStorage.setItem("user", JSON.stringify(user));
  }

  getCustomAnalytics(identifier: string, slider_id: number = null, algorithm_identifier=null): Observable<any> {
      let p = {"identifier": identifier};

      if(slider_id != null) {
        p["slider_id"] = slider_id;
      }

      if(algorithm_identifier != null) {
        p["algorithm_identifier"] = algorithm_identifier;
      }

      return this.http.get(this.baseURL + '/admin/analytics/custom', this.params(p));
  }

  getAllSliders(): Observable<any> {
    return this.http.get(this.baseURL + '/admin/sliders/all')
  }

  updateSlider(slider): Observable<any> {
    return this.http.post(this.baseURL + "/admin/sliders/edit", slider);
  }

  getSliderLocationURL(slider_id): Observable<any> {
    return this.http.get(this.baseURL + "/admin/sliders/location", this.params({"action": "prepare", "slider_id": slider_id}))
  }

  setSliderLocation(token, xpath): Observable<any> {
    return this.http.get(this.baseURL + "/admin/sliders/location", this.params({"action": "confirm", "token": token, "xpath": xpath}))
  }

  genShopifyAuthURL(shop): Observable<any> {
    return this.http.get(this.baseURL + "/shopify/get-auth", this.params({"shop":shop}));
  }

  getAllAlgorithms(): Observable<any> {
    return this.http.get(this.baseURL + "/admin/algorithms/all")
  }

  sendFeedback(message: string) {
    this.http.post(this.baseURL + "/admin/send-feedback", {"message": message}).subscribe(res => null);
  }

}
