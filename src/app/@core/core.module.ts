import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbAuthModule, NbPasswordAuthStrategy, NbAuthJWTToken } from '@nebular/auth';
import { NbSecurityModule, NbRoleProvider } from '@nebular/security';
import { of as observableOf } from 'rxjs';

import { throwIfAlreadyLoaded } from './module-import-guard';
import { DataModule } from './data/data.module';
import { AnalyticsService } from './utils/analytics.service';

const socialLinks = [
{
    url: 'https://github.com/akveo/nebular',
    target: '_blank',
    icon: 'socicon-github',
},
{
    url: 'https://www.facebook.com/akveo/',
    target: '_blank',
    icon: 'socicon-facebook',
},
{
    url: 'https://twitter.com/akveo_inc',
    target: '_blank',
    icon: 'socicon-twitter',
},
];

export class NbSimpleRoleProvider extends NbRoleProvider {
    getRole() {
    // here you could provide any role based on any auth flow
    return observableOf('guest');
}
}

export const NB_CORE_PROVIDERS = [
...DataModule.forRoot().providers,
...NbAuthModule.forRoot({

    strategies: [
    NbPasswordAuthStrategy.setup({
        name: 'email',
        token: {
            class: NbAuthJWTToken,
            key: "access_token",
        },
        baseEndpoint: 'https://api.butlerhub.io',
        // baseEndpoint: 'http://127.0.0.1:4567',
        login: {
            endpoint: '/admin/auth/login',
            method: 'post',
        },
        register: {
            endpoint: '/admin/auth/register',
            method: 'post',
        },
    }),
    ],
    forms: {
        login: {
              redirectDelay: 0, // delay before redirect after a successful login, while success message is shown to the user
              strategy: 'email',  // strategy id key.
              rememberMe: false,   // whether to show or not the `rememberMe` checkbox
              showMessages: {     // show/not show success/error messages
                  success: true,
                  error: true,
              }
          }
      },
  }).providers,

NbSecurityModule.forRoot({
    accessControl: {
        guest: {
            view: '*',
        },
        user: {
            parent: 'guest',
            create: '*',
            edit: '*',
            remove: '*',
        },
    },
}).providers,

{
    provide: NbRoleProvider, useClass: NbSimpleRoleProvider,
},
AnalyticsService,
];

@NgModule({
    imports: [
    CommonModule,
    ],
    exports: [
    NbAuthModule,
    ],
    declarations: [],
})
export class CoreModule {
    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
        throwIfAlreadyLoaded(parentModule, 'CoreModule');
    }

    static forRoot(): ModuleWithProviders {
        return <ModuleWithProviders>{
            ngModule: CoreModule,
            providers: [
            ...NB_CORE_PROVIDERS,
            ],
        };
    }
}
