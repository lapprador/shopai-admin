import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  // {
  //   title: 'Dashboard',
  //   icon: 'nb-home',
  //   link: '/p/dashboard',
  //   home: true,
  // },


  {
    title: 'Sliders',
    icon: 'nb-heart',
    children: [
      {
        title: "Overview",
        link: '/p/sliders/overview',
        home: true
      },
      // {
      //   title: "Performance",
      //   link: '/p/sliders/performance',
      // },
      {
        title: "Algorithms",
        link: '/p/sliders/algorithms',
      },
      {
        title: "New Slider",
        link: '/p/sliders/new',
      },
      // {
      //   title: "Edit Sliders",
      //   link: '/p/sliders/edit',
      // },
    ]
  },

  {
    title: 'Products',
    icon: 'nb-tables',
    children: [
      {
        title: "Overview",
        link: '/p/products/overview',
      },
      {
        title: "Performance",
        link: '/p/products/performance',
      },
      // {
      //   title: "Product Information",
      //   link: '/p/products/information',
      // },
      // {
      //   title: "Recommendations",
      //   link: '/p/products/recommendations',
      // },
    ]
  },

  // {
  //   title: 'Users (coming soon)',
  //   icon: 'nb-person',
  //   children: [
  //     {
  //       title: "Overview",
  //       link: "/p/users/overview",
  //     },
  //   ]
  // },

  // {
  //   title: 'Invitations',
  //   icon: 'nb-paper-plane',
  //   children: [
  //     {
  //       title: "Invite a friend",
  //       link: "/p/invitations/invite-friends",
  //     },
  //     {
  //       title: "Invitations",
  //       link: "/p/invitations"
  //     },
  //   ]
  // },

  {
    title: 'Settings',
    icon: 'nb-gear',
    children: [
      // {
      //   title: "Account",
      //   link: "/p/settings/account",
      // },
      {
        title: "Logout",
        link: "/p/settings/account",
      },
      // {
      //   title: "Pixel Setup",
      //   link: "/p/settings/pixel",
      // },
      // {
      //   title: "Payments",
      //   link: "/p/settings/payments",
      // },
    ]
  },

  // {
  //   title: 'Auth',
  //   icon: 'nb-locked',
  //   children: [
  //     {
  //       title: 'Login',
  //       link: '/auth/login',
  //     },
  //     {
  //       title: 'Register',
  //       link: '/auth/register',
  //     },
  //     {
  //       title: 'Request Password',
  //       link: '/auth/request-password',
  //     },
  //     {
  //       title: 'Reset Password',
  //       link: '/auth/reset-password',
  //     },
  //   ],
  // },
];
