import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsOverviewComponent } from './products-overview/products-overview.component';
import { ProductsPerformanceComponent } from './products-performance/products-performance.component';
import { ProductsRecommendationsComponent } from './products-recommendations/products-recommendations.component';

const routes: Routes = [
    {
        path: 'overview',
        component: ProductsOverviewComponent,
    },
    {
        path: 'performance',
        component: ProductsPerformanceComponent,
    },
    {
        path: 'recommendations',
        component: ProductsRecommendationsComponent,
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }

export const routedModules = [
    ProductsOverviewComponent,
    ProductsPerformanceComponent,
    ProductsRecommendationsComponent,
]
