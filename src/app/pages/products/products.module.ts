import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule, routedModules } from './products-routing.module';

import { ThemeModule } from '../../@theme/theme.module';

@NgModule({
  imports: [
    CommonModule,
    ProductsRoutingModule,
    ThemeModule.forRoot(),
  ],
  declarations: [...routedModules]
})
export class ProductsModule { }
