import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsRecommendationsComponent } from './products-recommendations.component';

describe('ProductsRecommendationsComponent', () => {
  let component: ProductsRecommendationsComponent;
  let fixture: ComponentFixture<ProductsRecommendationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsRecommendationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsRecommendationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
