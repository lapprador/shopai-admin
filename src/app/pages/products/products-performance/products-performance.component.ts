import { Component, OnInit, ViewChild } from '@angular/core';
import { TrafficList, TrafficListService } from '../../../@core/data/traffic-list.service';
import { takeWhile } from 'rxjs/operators';
import { TrafficBar, TrafficBarService } from '../../../@core/data/traffic-bar.service';

// For at bruge api
import { ApiService } from '../../../@core/data/api.service';
import { EchartsMultiComponent } from '../../../@theme/components/echarts/echarts-multi.component'

@Component({
  selector: 'ngx-products-performance',
  templateUrl: './products-performance.component.html',
  styleUrls: ['./products-performance.component.scss']
})
export class ProductsPerformanceComponent implements OnInit {

  @ViewChild('clicksPerProductBrand') clicksPerProductBrand: EchartsMultiComponent;
  @ViewChild('ctrPerBrand') ctrPerBrand: EchartsMultiComponent;
  @ViewChild('ctrPerCategory') ctrPerCategory: EchartsMultiComponent;
  @ViewChild('clicksPerProductCategory') clicksPerProductCategory: EchartsMultiComponent;
  @ViewChild('ctrPerSaving') ctrPerSaving: EchartsMultiComponent;
  @ViewChild('ctrPerPricepoint') ctrPerPricepoint: EchartsMultiComponent;





  constructor(private api: ApiService) { }


  ctr_per_pricepoint = []

  ngOnInit() {

      this.api.getCustomAnalytics("ctr_per_brand").subscribe(res => {
          this.ctrPerBrand.defaultAxis('category', 'value', null, null, true)
          this.ctrPerBrand.addSeries(this.ctrPerBrand.newSeries("CTR", "bar", res.map(x => { return [x.brand, x.ctr]}), true))
      });
      this.api.getCustomAnalytics("ctr_per_category").subscribe(res => {
        this.ctrPerCategory.defaultAxis('category','value', null, null, true)
        this.ctrPerCategory.addSeries(this.ctrPerCategory.newSeries("CTR", "bar", res.map(x => { return [x.category, x.ctr]}), true))
      });
      this.api.getCustomAnalytics("clicks_per_product_brand").subscribe(res => {

        this.clicksPerProductBrand.defaultAxis('category', 'value')
        this.clicksPerProductBrand.addSeries(this.clicksPerProductBrand.newSeries("Clicks","bar", res.map(x => { return [x.brand, x.clicks_per_product]})))

      });
      this.api.getCustomAnalytics("clicks_per_product_category").subscribe(res => {

        this.clicksPerProductCategory.defaultAxis("category", "value")
        this.clicksPerProductCategory.addSeries(this.clicksPerProductCategory.newSeries("Clicks","bar", res.map(x => { return [x.category, x.clicks_per_product]})))
      });
      this.api.getCustomAnalytics("ctr_per_saving").subscribe(res => {
        this.ctrPerSaving.defaultAxis('category', 'value', null, null, true)
        this.ctrPerSaving.addSeries(this.ctrPerSaving.newSeries("CTR", "bar", res.map(x => { return [x.saving, x.ctr]}), true))

      });
      this.api.getCustomAnalytics("ctr_per_pricepoint").subscribe(res => {
        this.ctrPerPricepoint.defaultAxis('category', 'value', null, null, true)
        this.ctrPerPricepoint.addSeries(this.ctrPerPricepoint.newSeries("CTR", "bar", res.map(x => { return [x.price_point, x.ctr]}), true))
      });

  }

  messages: any= [
    {
      text: 'I would love to hear your ideas and thoughts on new questions we could answer with data 😎 📊',
      date: new Date(),
      reply: false,
      user: {
        name: 'Andreas',
        avatar: './assets/images/andreas.png',
      },
    },
  ]

  sendMessage(event) {
     this.messages.push({
       text: event.message,
       date: new Date(),
       reply: true,
       user: {
         name: 'You',
         avatar: '',
       },
     });
     setTimeout(() => {
     	this.messages.push({
     	  text: "Thank you very much for your answer!",
     	  date: new Date(),
     	  reply: false,
     	  user: {
     	    name: 'Andreas',
     	    avatar: './assets/images/andreas.png',
     	  },
     	});
     	setTimeout(() => {
     		this.messages.push({
     		  text: "We have noted it, and will talk about the idea. Thanks for making Butlerhub better! 👏😍",
     		  date: new Date(),
     		  reply: false,
     		  user: {
     		    name: 'Andreas',
     		    avatar: './assets/images/andreas.png',
     		  },
     		});
     	}, 2000);
     }, 2000);

     this.api.sendFeedback(event.message);
   }


}
