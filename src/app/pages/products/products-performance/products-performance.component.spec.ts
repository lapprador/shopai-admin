import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsPerformanceComponent } from './products-performance.component';

describe('ProductsPerformanceComponent', () => {
  let component: ProductsPerformanceComponent;
  let fixture: ComponentFixture<ProductsPerformanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsPerformanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsPerformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
