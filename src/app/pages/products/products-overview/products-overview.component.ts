import { Component, OnInit, ViewChild } from '@angular/core';
import { TrafficList, TrafficListService } from '../../../@core/data/traffic-list.service';
import { takeWhile } from 'rxjs/operators';
import { TrafficBar, TrafficBarService } from '../../../@core/data/traffic-bar.service';

import { EchartsMultiComponent } from '../../../@theme/components/echarts/echarts-multi.component'

// For at bruge api
import { ApiService } from '../../../@core/data/api.service';

@Component({
  selector: 'ngx-products-overview',
  templateUrl: './products-overview.component.html',
  styleUrls: ['./products-overview.component.scss']
})
export class ProductsOverviewComponent implements OnInit {


  @ViewChild('productsPerPricepoint') productsPerPricepoint: EchartsMultiComponent;
  @ViewChild('productsPerCategory') productsPerCategory: EchartsMultiComponent;
  @ViewChild('productsPerBrand') productsPerBrand: EchartsMultiComponent;
  @ViewChild('productsPerSaving') productsPerSaving: EchartsMultiComponent;




    // For at bruge api
  constructor(private api: ApiService) { }


  ngOnInit() {
      // Ændr brand_count til den givne query identifier i databasen
      this.api.getCustomAnalytics("products_per_brand").subscribe(res => {

          // Do axis using shortcuts
          // this.productsPerBrand.addXAxis(
          //   this.productsPerBrand.newX('category', res.map(x => x.brand))
          // )

          // this.productsPerBrand.addYAxis(
          //   this.productsPerBrand.newY('value')
          // )

          // Manual x-axis
          // this.productsPerBrand.addXAxis(
          //     {
          //             type: 'category',
          //             axisTick: {
          //               alignWithLabel: true,
          //             },
          //             axisLine: {
          //               lineStyle: {
          //                 color: '#FF0000',
          //               },
          //             },
          //             axisLabel: {
          //               textStyle: {
          //                 color: '#000',
          //               },
          //             },
          //         }
          //  )

          // Do default
          // xType, yType, xValues(optional), yValues(optional), percentageOnY(optional)=false

          this.productsPerBrand.defaultAxis('category', 'value')

          var data =  res.map(x => [x.brand, x.count])
          this.productsPerBrand.addSeries(
            this.productsPerBrand.newSeries("Products", "bar", data),
            'linear'
          )
      });

      this.api.getCustomAnalytics("products_per_category").subscribe(res => {

        this.productsPerCategory.defaultAxis('category', 'value')
        this.productsPerCategory.addSeries(this.productsPerCategory.newSeries("Products","bar", res.map(x => { return [x.category, x.count]})))

      });

      this.api.getCustomAnalytics("products_per_pricepoint").subscribe(res => {

          this.productsPerPricepoint.defaultAxis('category', 'value')
          this.productsPerPricepoint.addSeries(this.productsPerPricepoint.newSeries("Products", "bar", res.map(x => { return [x.price_point, x.count]})))
      });
      this.api.getCustomAnalytics("products_per_saving").subscribe(res => {

          this.productsPerSaving.defaultAxis('category', 'value')
          this.productsPerSaving.addSeries(this.productsPerSaving.newSeries("Products", "bar", res.map( x => { return [x.saving, x.count]})))
      });

  }

  messages: any= [
    {
      text: 'I would love to hear your ideas and thoughts on new questions we could answer with data 😎 📊',
      date: new Date(),
      reply: false,
      user: {
        name: 'Andreas',
        avatar: './assets/images/andreas.png',
      },
    },
  ]

  sendMessage(event) {
     this.messages.push({
       text: event.message,
       date: new Date(),
       reply: true,
       user: {
         name: 'You',
         avatar: '',
       },
     });
     setTimeout(() => {
     	this.messages.push({
     	  text: "Thank you very much for your answer!",
     	  date: new Date(),
     	  reply: false,
     	  user: {
     	    name: 'Andreas',
     	    avatar: './assets/images/andreas.png',
     	  },
     	});
     	setTimeout(() => {
     		this.messages.push({
     		  text: "We have noted it, and will talk about the idea. Thanks for making Butlerhub better! 👏😍",
     		  date: new Date(),
     		  reply: false,
     		  user: {
     		    name: 'Andreas',
     		    avatar: './assets/images/andreas.png',
     		  },
     		});
     	}, 2000);
     }, 2000);

     this.api.sendFeedback(event.message);
   }


}
