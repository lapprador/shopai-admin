import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
  // {
  //   path: 'dashboard',
  //   component: ECommerceComponent,
  // },

  {
    path: 'sliders',
    loadChildren: './sliders/sliders.module#SlidersModule',
  },

  {
    path: 'products',
    loadChildren: './products/products.module#ProductsModule',
  },

  {
    path: 'users',
    loadChildren: './users/users.module#UsersModule',
  },

  {
    path: 'invitations',
    loadChildren: './invitations/invitations.module#InvitationsModule',
  },

  {
    path: 'settings',
    loadChildren: './settings/settings.module#SettingsModule',
  },

  {
    path: 'miscellaneous',
    loadChildren: './miscellaneous/miscellaneous.module#MiscellaneousModule',
  },
  {
    path: '',
    redirectTo: 'sliders',
    pathMatch: 'full',
  },
  {
    path: '**',
    component: NotFoundComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
