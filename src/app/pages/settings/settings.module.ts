import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule, routedModules } from './settings-routing.module';

import { ThemeModule } from '../../@theme/theme.module';

@NgModule({
  imports: [
    CommonModule,
    SettingsRoutingModule,
    ThemeModule.forRoot(),
  ],
  declarations: [...routedModules]
})
export class SettingsModule { }
