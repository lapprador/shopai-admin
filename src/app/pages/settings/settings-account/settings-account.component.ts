import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router'

@Component({
  selector: 'ngx-settings-account',
  templateUrl: './settings-account.component.html',
  styleUrls: ['./settings-account.component.scss']
})
export class SettingsAccountComponent implements OnInit {

  constructor(private router: Router) {

      localStorage.removeItem("auth_app_token");
      localStorage.removeItem("user");
      this.router.navigate(['/auth/login'])
  }

  ngOnInit() {}

}
