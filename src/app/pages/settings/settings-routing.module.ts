import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsAccountComponent } from './settings-account/settings-account.component';
import { SettingsPixelComponent } from './settings-pixel/settings-pixel.component';
import { SettingsPaymentsComponent } from './settings-payments/settings-payments.component';

const routes: Routes = [

    {
        path: 'account',
        component: SettingsAccountComponent,
    },
    {
        path: 'pixel',
        component: SettingsPixelComponent,
    },
    {
        path: 'payments',
        component: SettingsPaymentsComponent,
    },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }

export const routedModules = [
    SettingsAccountComponent,
    SettingsPixelComponent,
    SettingsPaymentsComponent,
]
