import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsPixelComponent } from './settings-pixel.component';

describe('SettingsPixelComponent', () => {
  let component: SettingsPixelComponent;
  let fixture: ComponentFixture<SettingsPixelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsPixelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsPixelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
