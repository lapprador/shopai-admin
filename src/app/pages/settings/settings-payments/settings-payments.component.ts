import { Component, OnInit } from '@angular/core';

// For at bruge api
import { ApiService } from 'app/@core/data/api.service';

@Component({
  selector: 'ngx-settings-payments',
  templateUrl: './settings-payments.component.html',
  styleUrls: ['./settings-payments.component.scss']
})
export class SettingsPaymentsComponent implements OnInit {

  // For at bruge api
  constructor(private api: ApiService) { }

  result = []

  ngOnInit() {
      // Ændr brand_count til den givne query identifier i databasen
      this.api.getCustomAnalytics("brand_count").subscribe(res => {
          // Ændr .brand og .count til det der skal være x og y
          this.result = res.map(x => { return {x: x.brand, y:x.count}})
      });
  }

}
