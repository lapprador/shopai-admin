import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsPaymentsComponent } from './settings-payments.component';

describe('SettingsPaymentsComponent', () => {
  let component: SettingsPaymentsComponent;
  let fixture: ComponentFixture<SettingsPaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsPaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
