import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvitationsRoutingModule, routedModules } from './invitations-routing.module';

import { ThemeModule } from '../../@theme/theme.module';

@NgModule({
  imports: [
    CommonModule,
    InvitationsRoutingModule,
    ThemeModule.forRoot(),
  ],
  declarations: [...routedModules]
})
export class InvitationsModule { }
