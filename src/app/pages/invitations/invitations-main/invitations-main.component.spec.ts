import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvitationsMainComponent } from './invitations-main.component';

describe('InvitationsMainComponent', () => {
  let component: InvitationsMainComponent;
  let fixture: ComponentFixture<InvitationsMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvitationsMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvitationsMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
