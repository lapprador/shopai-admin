import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvitationsInviteFriendComponent } from './invitations-invite-friend.component';

describe('InvitationsInviteFriendComponent', () => {
  let component: InvitationsInviteFriendComponent;
  let fixture: ComponentFixture<InvitationsInviteFriendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvitationsInviteFriendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvitationsInviteFriendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
