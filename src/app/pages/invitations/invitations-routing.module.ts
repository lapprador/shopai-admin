import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InvitationsInviteFriendComponent } from './invitations-invite-friend/invitations-invite-friend.component';
import { InvitationsMainComponent } from './invitations-main/invitations-main.component';

const routes: Routes = [
    {
        path: 'invite-friends',
        component: InvitationsInviteFriendComponent,
    },
    {
        path: '',
        component: InvitationsMainComponent,
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvitationsRoutingModule { }

export const routedModules = [
    InvitationsInviteFriendComponent,
    InvitationsMainComponent,
]
