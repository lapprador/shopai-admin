import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { EchartsMultiComponent } from '../../../@theme/components/echarts/echarts-multi.component'

import { ApiService } from '../../../@core/data/api.service';

@Component({
  selector: 'ngx-sliders-performance',
  templateUrl: './sliders-performance.component.html',
  styleUrls: ['./sliders-performance.component.scss']
})
export class SlidersPerformanceComponent implements OnInit {

  constructor(private route: ActivatedRoute, private api: ApiService) { }

  // @ViewChild('titleInput') titleInput: ElementRef;
  @ViewChild('clicksPerDay') clicksPerDay: EchartsMultiComponent;
  @ViewChild('productsASliderHasRecommended') productsASliderHasRecommended: EchartsMultiComponent;

  saveLoading = false;

  isActive = true;
  titleValue = '';

  slider = null
  startData = null
  productViews = null
  productClicks = null
  liveLink = "#"
  top10 = null

  getInformation(sliderID) {

    this.api.getAllSliders().subscribe(res => {
      let filteredSliders = res.filter(x => x.id == sliderID)
      this.slider = filteredSliders[0]
      console.log(this.slider)
      this.isActive = this.slider.active;
      this.titleValue = this.slider.title;

      this.api.getCustomAnalytics("slider_views_and_clicks", sliderID).subscribe(viewAndClickRes => {

        this.productViews = viewAndClickRes.filter(x => x.action_name === "product_view")[0]
        this.productClicks = viewAndClickRes.filter(x => x.action_name === "product_click")[0]
          // Got custom analytics for slider here
          // {{slider}}
        });

      this.api.getCustomAnalytics("link_to_most_used_page_for_slider", sliderID).subscribe(pageRes => {
        if(pageRes.length > 0) {
           this.liveLink = pageRes[0]['page']
        }
      })

      this.api.getCustomAnalytics("clicks_on_slider_per_day", sliderID).subscribe(clickRes => {
        console.log(clickRes)
        this.clicksPerDay.defaultAxis('category', 'value')
        this.clicksPerDay.addSeries(
          this.clicksPerDay.newSeries(
            "Clicks",
            "line",
            clickRes.map( x => { return [x.date.substring(0, 10), x.product_clicks]})
          )
        )
      })

      this.api.getCustomAnalytics("products_a_slider_has_recommended", sliderID).subscribe(pageRes => {
        this.top10 = pageRes.slice(0,10).map( x => {
          return {
            'product_id': x.product_id,
            'category': x.category,
            'brand': x.brand,
            'name': x.product_name,
            'url': x.url,
            'image': x.image,
            'recommendations': x.recommendations
         }
        })
      })

    });

  }

  ngOnInit() {
    this.route.paramMap.subscribe(res => {
      this.getInformation(res["params"]["id"])
    });
  }

  hasLiveLink() {
    return this.liveLink !== '#' && this.liveLink != null;
  }

  saveSlider() {
    if(this.saveLoading) {
      return
    }

    this.saveLoading = true;
    this.api.updateSlider(
        {
          "slider": {
            "title": this.titleValue,
            "active": this.isActive,
            "id": this.slider.id,
          }
        }
      ).subscribe(res => {
        this.saveLoading = false;
        this.slider = res;
      })
  }

}
