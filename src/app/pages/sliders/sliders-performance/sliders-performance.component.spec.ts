import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlidersPerformanceComponent } from './sliders-performance.component';

describe('SlidersPerformanceComponent', () => {
  let component: SlidersPerformanceComponent;
  let fixture: ComponentFixture<SlidersPerformanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlidersPerformanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlidersPerformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
