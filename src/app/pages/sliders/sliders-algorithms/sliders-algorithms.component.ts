import { Component, OnInit } from '@angular/core';

import { ApiService } from '../../../@core/data/api.service';

import { Router } from '@angular/router';

@Component({
  selector: 'ngx-sliders-algorithms',
  templateUrl: './sliders-algorithms.component.html',
  styleUrls: ['./sliders-algorithms.component.scss']
})
export class SlidersAlgorithmsComponent implements OnInit {

  categories = []
  algorithms = {}

	users: { name: string, title: string }[] = [
	    { name: 'Newton', title: 'Find products that was most often purchased with these' },
	    { name: 'Bob', title: 'Get the users taste and find most relevant products' },
	    { name: 'Janitor', title: 'The janitor removes all products the user most certaintly dont want to purchase' },
	    { name: 'Perry', title: 'V1 of our Neural network' },
	    { name: 'Ben', title: 'Ben take the most popular products' },
	  ];

  constructor(public api: ApiService, private router: Router) { }

  ngOnInit() {

    this.api.getAllAlgorithms().subscribe(res => {
      this.categories = res["categories"].reduce((rows, key, index) => (index % 3 == 0 ? rows.push([key]) : rows[rows.length-1].push(key)) && rows, []);
      this.algorithms = res["algorithms"];
    });

  }

  algorithmsForCategory(category) {
    if(!category) {
      return []
    }
    return category.algorithm_identifiers.map(x => this.algorithms[x]);
  }


  algorithm_click(user) {
  	console.log(user);
  	// window.open("https://www.google.com/search?q=" + user.name, "_blank");
  	window.open("http://localhost:4200/#/p/products/information", "_blank");
  }

}
