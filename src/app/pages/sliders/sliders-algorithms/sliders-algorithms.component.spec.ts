import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlidersAlgorithmsComponent } from './sliders-algorithms.component';

describe('SlidersAlgorithmsComponent', () => {
  let component: SlidersAlgorithmsComponent;
  let fixture: ComponentFixture<SlidersAlgorithmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlidersAlgorithmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlidersAlgorithmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
