import { Component, OnInit } from '@angular/core';


import { Router, ActivatedRoute } from '@angular/router';

import { ApiService } from '../../../@core/data/api.service';

@Component({
  selector: 'ngx-algorithm-information',
  templateUrl: './algorithm-information.component.html',
  styleUrls: ['./algorithm-information.component.scss'],
})
export class AlgorithmInformationComponent implements OnInit {

	  messages: any[]= [
	    {
	      text: 'How can we make this page better? 😎',
	      date: new Date(),
	      reply: false,
	      user: {
	        name: 'Yasir',
	        avatar: './assets/images/yasir.png',
	      },
	    },
	  ]

	  sendMessage(event) {
	     this.messages.push({
	       text: event.message,
	       date: new Date(),
	       reply: true,
	       user: {
	         name: 'You',
	         avatar: '',
	       },
	     });
	     setTimeout(() => {
	     	this.messages.push({
	     	  text: "Thank you very much for your answer!",
	     	  date: new Date(),
	     	  reply: false,
	     	  user: {
	     	    name: 'Yasir',
	     	    avatar: './assets/images/yasir.png',
	     	  },
	     	});
	     	setTimeout(() => {
	     		this.messages.push({
	     		  text: "We have noted it down, and will talk about the idea. Thanks for making Butlerhub better! 👏😍",
	     		  date: new Date(),
	     		  reply: false,
	     		  user: {
	     		    name: 'Yasir',
	     		    avatar: './assets/images/yasir.png',
	     		  },
	     		});
	     	}, 2000);
	     }, 2000);

	   }

  constructor(private route: ActivatedRoute, private api: ApiService) { }

  algorithm = null;

  algorithmDetails = null;

  getInformation(algoIdent) {
  	this.api.getAllAlgorithms().subscribe(res => {
  		console.log(Object.values(res.algorithms))
  		this.algorithm = Object.values(res.algorithms).filter(x => x["identifier"] === algoIdent)[0];
  		console.log(this.algorithm);
  	});

  	this.api.getCustomAnalytics("algorithm_details", null, algoIdent).subscribe(res => {

  	  this.algorithmDetails = res[0];
  	  console.log(this.algorithmDetails);
  	})

	}

  ngOnInit() {
  	this.route.paramMap.subscribe(res => {
      this.getInformation(res["params"]["identifier"])
    });


  }

}
