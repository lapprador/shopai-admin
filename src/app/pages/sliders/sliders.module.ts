import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SlidersRoutingModule, routeModules } from './sliders-routing.module';

import { ThemeModule } from '../../@theme/theme.module';


@NgModule({
  imports: [
    CommonModule,
    SlidersRoutingModule,
    ThemeModule.forRoot(),
  ],
  declarations: [...routeModules]
})
export class SlidersModule { }
