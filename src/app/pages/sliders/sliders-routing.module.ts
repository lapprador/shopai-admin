import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SlidersOverviewComponent } from './sliders-overview/sliders-overview.component';
import { SlidersPerformanceComponent } from './sliders-performance/sliders-performance.component';
import { SlidersAlgorithmsComponent } from './sliders-algorithms/sliders-algorithms.component';
import { SlidersNewComponent } from 'app/@theme/components';
import { SlidersEditComponent } from './sliders-edit/sliders-edit.component';

import { AlgorithmInformationComponent } from './algorithm-information/algorithm-information.component';

const routes: Routes = [
    {
        path: 'overview',
        component: SlidersOverviewComponent,
    },
    {
        path: 'overview/:id',
        component: SlidersPerformanceComponent,
    },
    {
        path: 'algorithms',
        component: SlidersAlgorithmsComponent,
    },
    {
        path: 'algorithms/:identifier',
        component: AlgorithmInformationComponent,
    },
    {
        path: 'new',
        component: SlidersNewComponent,
    },
    {
        path: 'edit',
        component: SlidersEditComponent,
    },
    {
      path: '',
      redirectTo: 'overview',
      pathMatch: 'full',
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SlidersRoutingModule { }

export const routeModules = [
    SlidersOverviewComponent,
    SlidersPerformanceComponent,
    SlidersAlgorithmsComponent,
    // SlidersNewComponen
    AlgorithmInformationComponent,
    SlidersEditComponent,
]
