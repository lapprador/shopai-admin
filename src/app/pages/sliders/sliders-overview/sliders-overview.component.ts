import { Component, OnInit } from '@angular/core';

import { ApiService } from '../../../@core/data/api.service';

@Component({
  selector: 'ngx-sliders-overview',
  templateUrl: './sliders-overview.component.html',
  styleUrls: ['./sliders-overview.component.scss']
})
export class SlidersOverviewComponent implements OnInit {

  constructor(private api: ApiService) { }

  sliders = []
  loading = true;

  ngOnInit() {

      this.api.getAllSliders().subscribe(res => {
          this.sliders = res;
          this.loading = false;
      });

  }

  sliderClicked(slider) {
      console.log("clicked");
  }

  createNewCTA() {

  }

}
