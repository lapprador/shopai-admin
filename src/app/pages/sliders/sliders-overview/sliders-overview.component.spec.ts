import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlidersOverviewComponent } from './sliders-overview.component';

describe('SlidersOverviewComponent', () => {
  let component: SlidersOverviewComponent;
  let fixture: ComponentFixture<SlidersOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlidersOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlidersOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
