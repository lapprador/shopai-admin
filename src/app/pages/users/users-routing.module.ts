import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersOverviewComponent } from './users-overview/users-overview.component';

const routes: Routes = [
    {
        path: "overview",
        component: UsersOverviewComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
export const routedModules = [
    UsersOverviewComponent,
]
