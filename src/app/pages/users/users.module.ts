import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule, routedModules } from './users-routing.module';

import { ThemeModule } from '../../@theme/theme.module';

@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    ThemeModule.forRoot(),

  ],
  declarations: [...routedModules]
})
export class UsersModule { }
