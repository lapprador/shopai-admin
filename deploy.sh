#!/bin/sh
echo "Bulding project..."
ng build --prod --aot --build-optimizer

echo "Deploying to server"
rsync -az --progress --stats -e 'ssh' ./dist/ root@api.butlerhub.io:/var/www/admin

echo "Deploy done"

